enum mode {
	MODE_NORMAL,
	MODE_VISUAL,
	MODE_READLINE,
	MODE_LAST
};

struct sfm {
	struct path *path;
	struct dir **dirs;
	const unsigned int *ratios;
	size_t dsize;
	struct preview *preview;
	enum mode mode;
	/* TODO: keybuf for `showcmd` future */
};
