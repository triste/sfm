static const unsigned column_ratios[] = { 2, 3, 3 };

static const struct keybinding bindings_normal[] = {
        { "h", moveleft, {0} },
        { "j", movedown, {0} },
        { "k", moveup, {0} },
        { "l", moveright, {0} },
        { "o", openfile, {.s = "xdg-open"} },
        { "e", editfile, {.s = NULL} },
        { "q", quit, {0} },
};

/* alligment this via `s/,/,             /g` and Ctrl_V column then `20<` */
