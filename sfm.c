#include <unistd.h> /* execlp(), fork() */
#include <signal.h> /* sigaction(), SIGWINCH, SIGTERM, sig_atomic_t */
#include <sys/wait.h> /* waitpid() */
#include <stdlib.h> /* getenv() */

#include "path.h"
#include "fs.h"
#include "preview.h"
#include "ui.h"
#include "util.h"
#include "map.h"
#include "sfm.h"

union arg {
	int i;
	unsigned int u;
	const char *s;
};

struct keybinding {
	const char *key;
	void (*func)(union arg *);
	union arg arg;
};

static void setup(void);
static void run(void);
static void cleanup(void);
static void sighandler(int);

static void updatepreview(void);
static void spawn(const char *, const char *);

static void movedown(union arg *);
static void moveup(union arg *);
static void moveleft(union arg *);
static void moveright(union arg *);
static void openfile(union arg *);
static void editfile(union arg *);
static void quit(union arg *);

static volatile sig_atomic_t sigwinch = 1;
static volatile sig_atomic_t sigterm;
static struct ui *ui;
static struct fs *fs;
static struct path *tempath;
static size_t count;
static struct sfm sfm;
static const char *editor;
static struct map *bindings[MODE_LAST];

#define COLUMN_LAST (LENGTH(column_ratios) - 1)
#define COLUMN_MAIN (MIN(COLUMN_LAST, COLUMN_LAST - 1))

#include "config.h"

void
movedown(union arg *arg)
{
	struct dir *dir = sfm.dirs[COLUMN_MAIN];
	size_t sub;

	(void)arg;

	sub = dir->fsize - 1 - dir->cfile;

	if (sub > count) {
		dir->cfile += count;
		dir->cline += count;
	} else if (sub) {
		dir->cfile += sub;
		dir->cline += sub;
	} else {
		return;
	}

	updatepreview();
}

void
moveup(union arg *arg)
{
	struct dir *dir = sfm.dirs[COLUMN_MAIN];

	(void)arg;

	if (dir->cfile > count) {
		dir->cfile -= count;
		dir->cline = dir->cline > count ? dir->cline - count : 0;
	} else if (dir->cfile) {
		dir->cfile = dir->cline = 0;
	} else {
		return;
	}
	
	updatepreview();
}

void
moveleft(union arg *arg)
{
	struct dir *dir;
	size_t i;

	(void)arg;

	if (path_parent(sfm.path) == NULL)
		return;

	dir = sfm.dirs[0];
	fs_unwatch(fs, sfm.dirs[COLUMN_LAST]);
	preview_set(sfm.preview, NULL, NULL);

	for (i = COLUMN_LAST; i != 0; --i)
		sfm.dirs[i] = sfm.dirs[i-1];

	path_copy(tempath, sfm.path);
	for (i = 0; i != COLUMN_MAIN; ++i) {
		if (path_parent(tempath) == NULL) {
			sfm.dirs[0] = NULL;
			return;
		}
	}
	sfm.dirs[0] = fs_watch(fs, tempath->buf);
	dir_setcfile(sfm.dirs[0], dir->ino, dir->dev);
}

void
moveright(union arg *arg)
{
	struct dir *dir;
	struct file *file;
	unsigned int i;

	(void)arg;

	dir = sfm.dirs[COLUMN_MAIN];
	if (dir->fsize == 0)
		return;

	file = dir->files + dir->cfile;
	if (!S_ISDIR(file->mode) && !S_ISLNK(file->mode))
		return;

	path_join(sfm.path, file->name);
	if ((dir = fs_watch(fs, sfm.path->buf)) == NULL) {
		path_parent(sfm.path);
		return; /* Link to a nonexistent directory */
	}

	fs_unwatch(fs, sfm.dirs[0]);
	for (i = 0; i != COLUMN_MAIN; ++i)
		sfm.dirs[i] = sfm.dirs[i+1];
	sfm.dirs[COLUMN_MAIN] = dir;

	updatepreview();
}

void
editfile(union arg *arg)
{
	union arg a;

	a.s = arg->s ? arg->s : editor;
	openfile(&a);
}

void
openfile(union arg *arg)
{
	struct dir *dir = sfm.dirs[COLUMN_MAIN];

	if (!arg->s)
		return;

	path_copy(tempath, sfm.path);
	path_join(tempath, dir->files[dir->cfile].name);
	spawn(arg->s, tempath->buf);
}

void
quit(union arg *arg)
{
	(void)arg;

	sigterm = 1;
}

void
updatepreview(void)
{
	struct dir *dir;
	struct file *file;

	if (COLUMN_LAST == COLUMN_MAIN)
		return;

	fs_unwatch(fs, sfm.dirs[COLUMN_LAST]);
	sfm.dirs[COLUMN_LAST] = NULL;
	preview_set(sfm.preview, NULL, NULL);

	dir = sfm.dirs[COLUMN_MAIN];
	if (dir->fsize == 0)
		return;

	file = dir->files + dir->cfile;
	if (S_ISDIR(file->mode) || S_ISLNK(file->mode)) {
		path_copy(tempath, sfm.path);
		path_join(tempath, file->name);
		sfm.dirs[COLUMN_LAST] = fs_watch(fs, tempath->buf);
	} else {
		preview_set(sfm.preview, sfm.path, file->name);
	}
}

void
spawn(const char *file, const char *arg)
{
	pid_t pid;
	int status;

	ui_suspend(ui);
	if ((pid = fork()) == 0) {
		execlp(file, file, arg, NULL);
		die("execlp\n");
	} else {
		while (waitpid(pid, &status, 0) == -1);
	}
	ui_resume(ui);
}

void
sighandler(int signal)
{
	switch (signal) {
	case SIGWINCH:
		sigwinch = 1;
		break;
	case SIGTERM:
		sigterm = 1;
		break;
	}
}



void
handleinput(void)
{
	const char *key;
	struct keybinding *kb;
	static struct map suffix;
	static struct map *map = NULL;

	if (map == NULL)
		map = bindings[sfm.mode];

	if ((key = ui_getkey(ui)) == NULL)
		return;

	if (sfm.mode == MODE_NORMAL && *key >= '0' && *key <= '9') {
		count = count * 10 + *key - '0';
		return;
	}

	if ((kb = map_find(map, key, &suffix)) != NULL) {
		if (!map_isempty(&suffix)) {
			/* XXX: execute on timeout */
		}
		count = MAX(count, 1);
		kb->func(&kb->arg);
	} else if (!map_isempty(&suffix)) {
		map = &suffix;
		/* TODO: append key to buffer for `showcmd` future */
		return;
	} else {
		/* XXX: default action associated with current mode */
	}
	map = NULL;
	count = 0;
}

#include <sys/inotify.h>
#include <errno.h>
#include <stdio.h>

void
handleinotify(void)
{
	char buf[4096];
	ssize_t n;
	const char *ptr;
	const struct inotify_event *event;

	n = read(fs->fd, buf, sizeof buf);
	if (n == -1 && errno != EAGAIN) {
		perror("read");
		exit(1);
	}

	if (n <= 0)
		return;

	for (ptr = buf; ptr < buf + n;
			ptr += sizeof(struct inotify_event) + event->len) {
		event = (const struct inotify_event *) ptr;
		if (event->mask & IN_CREATE)
			fprintf(stderr, "IN_CREATE\n");
		if (event->mask & IN_DELETE)
			fprintf(stderr, "IN_DELETE\n");

		fprintf(stderr, "wd: %i\n", event->wd);
		if (event->len)
			fprintf(stderr, event->name);

		if (event->mask & IN_ISDIR)
			fprintf(stderr, " [directory]\n");
		else
			fprintf(stderr, " [file]\n");

	}
}

#include <sys/select.h>

void
run(void)
{
	fd_set fdset;

	updatepreview();
	ui_resume(ui);

	for (;;) {
		if (sigwinch) {
			sigwinch = 0;
			ui_resize(ui);
		}
		ui_draw(ui);
		if (sigwinch)
			continue;
		if (sigterm)
			break;
		FD_ZERO(&fdset);
		FD_SET(STDIN_FILENO, &fdset);
		FD_SET(fs->fd, &fdset);
		select(MAX(fs->fd, STDIN_FILENO) + 1, &fdset, 0, 0, 0);

		if (FD_ISSET(STDIN_FILENO, &fdset))
			handleinput();
		if (FD_ISSET(fs->fd, &fdset))
			handleinotify();
	}

	ui_suspend(ui);
}

void
run_(void)
{
	const char *key;
	struct keybinding *kb;
	static struct map suffix;
	struct map *map = bindings[sfm.mode];;

	updatepreview();
	ui_resume(ui);

	for (;;) {
		if (sigwinch) {
			sigwinch = 0;
			ui_resize(ui);
		}
		ui_draw(ui);
		if (sigwinch)
			continue;
		if (sigterm)
			break;
		/* XXX: block signals to avoid race? */
		if ((key = ui_getkey(ui)) == NULL)
			continue;

		if (sfm.mode == MODE_NORMAL && *key >= '0' && *key <= '9') {
			count = count * 10 + *key - '0';
			continue;
		}

		if ((kb = map_find(map, key, &suffix)) != NULL) {
			if (!map_isempty(&suffix)) {
				/* XXX: execute on timeout */
			}
			count = MAX(count, 1);
			kb->func(&kb->arg);
		} else if (!map_isempty(&suffix)) {
			map = &suffix;
			/* TODO: append key to buffer for `showcmd` future */
			continue;
		} else {
			/* XXX: default action associated with current mode */
		}
		map = bindings[sfm.mode];
		count = 0;
	}

	ui_suspend(ui);
}

void
setup(void)
{
	struct path *path;
	struct preview *preview;
	static struct dir *dirs[LENGTH(column_ratios)];
	size_t i;

	if (xsignal(SIGWINCH, sighandler) == -1 || xsignal(SIGTERM, sighandler) == -1)
		die("Failed to set signal handler\n");

	bindings[MODE_NORMAL] = map_create();
	for (i = 0; i < LENGTH(bindings_normal); ++i)
		map_insert(bindings[MODE_NORMAL], bindings_normal[i].key, &bindings_normal[i]);

	if ((editor = getenv("EDITOR")) == NULL)
		editor = "vi";

	fs = fs_create();

	if ((path = path_at(".")) == NULL && (path = path_at("/")) == NULL)
		die("Can't init path\n");

	while ((dirs[COLUMN_MAIN] = fs_watch(fs, path->buf)) == NULL)
		if (path_parent(path) == NULL)
			die("Can't load any directory\n");

	tempath = path_dup(path);
	for (i = COLUMN_MAIN; i > 0; --i) {
		if (path_parent(tempath) == NULL)
			break;
		dirs[i-1] = fs_watch(fs, tempath->buf);
		dir_setcfile(dirs[i-1], dirs[i]->ino, dirs[i]->dev);
	}

	preview = preview_create(NULL);

	sfm.path = path;
	sfm.dirs = dirs;
	sfm.ratios = column_ratios;
	sfm.dsize = LENGTH(column_ratios);
	sfm.preview = preview;
	ui = ui_create(&sfm);
}

void
cleanup(void)
{
	ui_free(ui);
	preview_free(sfm.preview);
	path_free(tempath);
	path_free(sfm.path);
	fs_free(fs);
	map_free(bindings[MODE_NORMAL]);
}

int
main(void)
{
	setup();
	run();
	cleanup();

	return 0;
}
