#include <stdlib.h> /* free */
#include <string.h> /* memset() */
#include <unistd.h> /* read(), close(), ssize_t */
#include <fcntl.h> /* open(), O_RDONLY */

#include <ctype.h> /* iscntrl(), isspace() */

#include "path.h"
#include "util.h"

#include "preview.h"

struct preview *
preview_create(const char *viewer)
{
	struct preview *preview;
	struct path *path;

	(void)viewer; /* TODO: implement custom viewer */

	path = xcalloc(1, sizeof(struct path));
	preview = xcalloc(1, sizeof(struct preview));
	preview->path = path;
	return preview;
}

void
preview_free(struct preview *preview)
{
	free(preview->buffer);
	free(preview->lines);
	path_free(preview->path);
	free(preview);
}

void
preview_set(struct preview *preview, struct path *path, const char *file)
{
	if (path == NULL) {
		preview->isloadable = 0;
		return;
	}
	path_copy(preview->path, path);
	preview->isloadable = 1;
	if (file != NULL) {
		path_join(preview->path, file);
	}
}

size_t
preview_preload(struct preview *preview, size_t lines)
{
	ssize_t i, n;
	int fd;
	size_t bsize;
	char *str;
	size_t line;

	if (!preview->isloadable) {
		return 0;
	}

	bsize = MAX(256, lines * 100);
	if (preview->bcap < bsize) {
		preview->buffer = xrealloc(preview->buffer, bsize);
		preview->bcap = bsize;
	}
	fd = open(preview->path->buf, O_RDONLY);
	n = read(fd, preview->buffer, 256);
	for (i = 0; i < n; ++i) {
		if (iscntrl(preview->buffer[i]) && !isspace(preview->buffer[i])) {
			preview->isloadable = 0;
			close(fd);
			return 0;
		}
	}
	n += read(fd, preview->buffer + n, bsize - n);
	if (preview->lcap < lines) {
		preview->lines = xrealloc(preview->lines, lines * sizeof(char *));
		preview->lcap = lines;
	}

	for (str = preview->buffer, i = line = 0; i < n; ++i) {
		if (preview->buffer[i] == '\n') {
			preview->buffer[i] = '\0';
			preview->lines[line] = str;
			if (line + 1 >= lines) {
				break;
			}
			line++;
			str = preview->buffer + i + 1;
		}
	}
	close(fd);
	return line;
}
