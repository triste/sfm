#include <stddef.h> /* NULL */
#include <stdlib.h> /* free() */
#include <stdint.h> /* SIZE_MAX */

#include "hash.h"
#include "util.h"

#define HT_MINSIZE	(1UL << 4)
#define HI_REMOVED	((void *)ht)
#define INDEX_NONE	(ht->size)

/* TODO: cleanup */


struct htable *
ht_create(struct htconf *conf, unsigned int size)
{
	struct htable *ht;

	ht = xmalloc(sizeof(struct htable));
	if ((ht->size = 1UL << size) < HT_MINSIZE)
		ht->size = HT_MINSIZE;
	ht->data = xcalloc(ht->size, sizeof(*ht->data));
	ht->used = 0;
	ht->conf = *conf;
	return ht;
}

void
ht_resize(struct htable *ht)
{
	void **data;
	size_t size;
	size_t i, j;
	void *entry;

	size = ht->size >= SIZE_MAX >> 1UL ? SIZE_MAX : ht->size << 1UL;
	data = xcalloc(size, sizeof(void *));

	for (entry = ht_first(ht, &i); entry != NULL; entry = ht_next(ht, &i)) {
		j = ht->conf.hash_f(entry) & (size - 1);
		while (data[j] != NULL) {
			if (++j == size) {
				j = 0;
			}
		}
		data[j] = entry;
	}
	free(ht->data);
	ht->data = data;
	ht->size = size;
}

void
ht_cleanup(struct htable *ht)
{
	free(ht->data);
}

void
ht_free(struct htable *ht)
{
	free(ht->data);
	free(ht);
}

void *
ht_find(struct htable *ht, size_t i)
{
	return ht->data[i] == HI_REMOVED ? NULL : ht->data[i];
}

void
ht_insert(struct htable *ht, size_t i, void *data)
{
	ht->data[i] = data;
	if (++ht->used * 4 > ht->size * 3)
		ht_resize(ht);
}

void
ht_remove(struct htable *ht, size_t i)
{
	if (ht->data[i] != NULL && ht->data[i] != HI_REMOVED) {
		ht->used--;
		ht->data[i] = HI_REMOVED;
	}
}

size_t
ht_lookup(struct htable *ht, void *data)
{
	size_t hv;
	size_t i;
	size_t empty;

	hv = ht->conf.hash_f(data);
	i = hv & (ht->size - 1);

	if (ht->data[i] == NULL) {
		return i;
	}

	if (ht->data[i] == HI_REMOVED)
		empty = i;
	else if (ht->conf.cmp_f(ht->data[i], data))
		return i;
	else
		empty = INDEX_NONE;

	for (;;) {
		/* TODO: find a more effective step than 1 */
		if (++i == ht->size)
			i = 0;

		if (ht->data[i] == NULL) {
			if (empty != INDEX_NONE) {
				return empty;
			}
			return i;
		}

		if (ht->data[i] == HI_REMOVED && empty == INDEX_NONE)
			empty = i;
		else if (ht->conf.cmp_f(ht->data[i], data)) {
			if (empty != INDEX_NONE) {
				ht->data[empty] = ht->data[i];
				ht->data[i] = HI_REMOVED;
				return empty;
			} else {
				return i;
			}
		}
	}
}

void *
ht_first(struct htable *ht, size_t *i)
{
	*i = 0;

	return ht_next(ht, i);
}

void *
ht_next(struct htable *ht, size_t *i)
{
	for (; *i < ht->size; (*i)++)
		if (ht->data[*i] != HI_REMOVED && ht->data[*i] != NULL)
			return ht->data[(*i)++];

	return NULL;
}
