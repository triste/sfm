/* XXX: implement compressed trie instead? */

struct map;

struct node {
	char key;
	const void *value;
	struct map *next;
};

/* edges holder  */
struct map {
	struct node *nodes; /* sorted by key */
	size_t num;
};

struct map *map_create(void);
void map_free(struct map *);

void map_insert(struct map *, const char *, const void *);
void *map_find(const struct map *, const char *, struct map *);

#define map_isempty(x) ((x)->num == 0)
