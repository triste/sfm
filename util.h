#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))
#define LENGTH(X)	(sizeof(X) / sizeof(X)[0])

void die(const char *, ...);
void *xmalloc(size_t);
void *xcalloc(size_t, size_t);
void *xrealloc(void *, size_t);
char *xstrdup(const char *);
int xsignal(int, void (*)(int));
