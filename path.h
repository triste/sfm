struct path {
	char *buf;
	size_t cap;
	size_t len;
};

struct path *path_at(const char *);
struct path *path_cwd(void);
struct path *path_dup(const struct path *);

void path_free(struct path *);

void path_copy(struct path *, const struct path *);
struct path *path_parent(struct path *);
void path_join(struct path *, const char *);
