struct preview {
	struct path *path;

	char *buffer;
	size_t bsize;
	size_t bcap;

	char **lines;
	size_t lsize;
	size_t lcap;

	int isloadable;
};

struct preview *preview_create(const char *);
void preview_free(struct preview *);

void preview_set(struct preview *, struct path *, const char *);

size_t preview_preload(struct preview *, size_t lines);
