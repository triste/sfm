#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <signal.h> /* sigaction(), SIGWINCH */

#include "util.h"

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	exit(1);
}

void *
xmalloc(size_t size)
{
	void *ptr;

	if ((ptr = malloc(size)) == NULL)
		die("sfm is out of memory!");

	return ptr;
}

void *
xcalloc(size_t nmemb, size_t size)
{
	void *ptr;

	if ((ptr = calloc(nmemb, size)) == NULL)
		die("sfm is out of memory!");

	return ptr;
}

void *
xrealloc(void *ptr, size_t size)
{
	if ((ptr = realloc(ptr, size)) == NULL)
		die("sfm is out of memory!");

	return ptr;
}

char *
xstrdup(const char *str)
{
	char *dst;
	size_t siz;

	siz = strlen(str) + 1;
	dst = xmalloc(siz);
	memcpy(dst, str, siz);

	return dst;
}


int
xsignal(int sig, void (*func)(int))
{
	struct sigaction sa;

	sa.sa_handler = func;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;

	return sigaction(sig, &sa, NULL);
}
