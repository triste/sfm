#include <stdlib.h>

#include "map.h"
#include "util.h"

struct map *
map_create(void)
{
	struct map *map;

	map = xcalloc(1, sizeof(struct map));
	return map;
}

void
map_free(struct map *map)
{
	size_t i;

	for (i = 0; i < map->num; ++i)
		if (map->nodes[i].next != NULL)
			map_free(map->nodes[i].next);
	free(map->nodes);
	free(map);
}

static int
compnode(const void *n1, const void *n2)
{
	const struct node *a = (const struct node *)n1;
	const struct node *b = (const struct node *)n2;

	if (a->key < b->key) {
		return -1;
	}
	if (a->key == b->key) {
		return 0;
	}
	return 1;
}

void
map_insert(struct map *map, const char *prefix, const void *data)
{
	struct map *next = map;
	struct node *node = map->nodes;
	const char *ch;
	size_t i;

	for (ch = prefix; *ch != '\0'; ++ch) {
		node = bsearch(ch, next->nodes, next->num, sizeof(struct node), compnode);
		if (node == NULL) {
			next->num++;
			next->nodes = xrealloc(next->nodes, next->num * sizeof(struct node));
			for (i = next->num - 1; i > 0 && next->nodes[i-1].key > *ch; --i)
				next->nodes[i] = next->nodes[i-1];
			node = next->nodes + i;
			node->key = *ch;
			node->value = node->next = NULL;
		}
		if (node->next == NULL && ch[1] != '\0')
			node->next = map_create();
		next = node->next;
	}
	node->value = data;
}

void *
map_find(const struct map *map, const char *prefix, struct map *suffix)
{
	const struct map *next = map;
	struct node *node = NULL;
	const char *ch;

	for (ch = prefix; *ch != '\0'; ++ch) {
		if (next == NULL || next->nodes == NULL) {
			suffix->nodes = NULL;
			suffix->num = 0;
			return NULL;
		}
		node = bsearch(ch, next->nodes, next->num, sizeof(struct node), compnode);
		if (node == NULL) {
			suffix->nodes = NULL;
			suffix->num = 0;
			return NULL;
		}
		next = node->next;
	}
	if (next != NULL) {
		*suffix = *next;
	} else {
		suffix->nodes = NULL;
		suffix->num = 0;
	}
	return node ? (void *)node->value : NULL;
}
