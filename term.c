#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h> /* open */
#include <stddef.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>

#include "ui.h"
#include "util.h"

struct term {
	struct termios torig;
	struct termios traw;
	char *buf;
	int fd;
};

static int setty(int, int, const struct termios *);

struct term *
term_create(void)
{
	struct term *term;
	struct termios torig, traw;
	int fd;

	fd = STDIN_FILENO;
	if (isatty(fd) == 0)
		die("Not a tty\n");

	if (tcgetattr(fd, &torig) == -1)
		die("Can't get terminal attributes: %s\n", strerror(errno));

	traw = torig;
	/* fix ctrl-m, disable ctrl-[sq] */
	traw.c_iflag &= ~(ICRNL | IXON);
	/* disable echo, canon, ctrl-[cz] */
	traw.c_lflag &= ~(ECHO | ICANON | ISIG);
	/* blocking read */
	traw.c_cc[VMIN] = 1;
	traw.c_cc[VTIME] = 0;

	term = xmalloc(sizeof(struct term));
	term->torig = torig;
	term->traw = traw;
	term->buf = NULL;
	term->fd = fd;
	return term;
}

void
term_free(struct term *term)
{
	free(term->buf);
	free(term);
}

void
term_raw(struct term *term, int raw)
{
	if (raw) {
		if (setty(term->fd, TCSANOW, &term->traw) == -1)
			die("Can't set the terminal to the raw mode: %s\n",
					strerror(errno));
		(void) write(term->fd, "\x1b[?25l", 6);
	} else {
		if (setty(term->fd, TCSANOW, &term->torig) == -1)
			die("Can't restore the terminal: %s\n",
					strerror(errno));
		(void) write(term->fd, "\x1b[?25h", 6);
	}
}

void
term_size(struct term *term, unsigned short *rows, unsigned short *cols)
{
	struct winsize ws;

	while (ioctl(term->fd, TIOCGWINSZ, &ws) == -1) {
		if (errno = EINTR)
			continue;
		die("Can't get terminal size: %s\n", strerror(errno));
	}
	*rows = ws.ws_row;
	*cols = ws.ws_col;
}

void
term_render(struct term *term, const struct cell *cels, size_t num)
{
	char *bi;
	size_t i;

	term->buf = xrealloc(term->buf, num * sizeof(cels->data));
	strcpy(term->buf, "\x1b[H");
	for (bi = term->buf + 3, i = 0; i < num;) {
		if (cels[i].len == 0) {
			*bi++ = ' ';
			i++;
		} else {
			strncpy(bi, cels[i].data, cels[i].len);
			bi += cels[i].len;
			i += cels[i].width;
		}
	}
	write(term->fd, term->buf, bi - term->buf);
}

char
term_getchar(struct term *term)
{
	char ch;

	while (read(term->fd, &ch, sizeof(char)) == -1) {
		if (errno == EAGAIN)
			continue;
		if (errno == EINTR)
			return EOF;
		die("Can't get key: %s\n", strerror(errno));
	}
	return ch;
}

int
setty(int fd, int action, const struct termios *ti)
{
	int ret;

	while ((ret = tcsetattr(fd, action, ti)) == -1 && errno == EINTR)
		continue;
	return ret;
}
