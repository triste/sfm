#include <limits.h>

struct cell {
	char data[MB_LEN_MAX];
	int len;
	int width;
};

struct window {
	int x, y;
	unsigned short w, h;
	struct cell **lines;
};

struct column {
	struct window win;
	unsigned short curline;
};

struct ui {
	struct sfm *sfm;
	struct term *term;

	struct window *wins;

	struct cell *cells;
	size_t cnum;
};

struct ui *ui_create(struct sfm *);
void ui_free(struct ui *);

void ui_suspend(struct ui *);
void ui_resume(struct ui *);

const char *ui_getkey(struct ui *);

void ui_draw(struct ui *);
int ui_resize(struct ui *);
