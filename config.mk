# sfm version
VERSION = 0.1

# includes and libs
#LIBS = -ltermbox

# flags
#
# __USE_XOPEN2K8 == _XOPEN_SOURCE >= 700 || _POSIX_C_SOURCE >= 200809L
#
# ino_t, dev_t mode_t
# _XOPEN_SOURCE || __USE_XOPEN2K
#
# fstatat()
# __USE_XOPEN2K8
#
# strdup()
# __USE_XOPEN2K8
#
# dirfd()
# __USE_XOPEN2K8
#
# wcwidth()
# _XOPEN_SOURCE
#CPPFLAGS = -D_XOPEN_SOURCE=700
# ||
#
# realpath()
# _XOPEN_SOURCE>= 500
CPPFLAGS = -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=500
CFLAGS += -std=c89 -Wall -Wextra -Wpedantic -Os ${CPPFLAGS}
LDFLAGS = ${LIBS}
