#include <string.h> /* strlen() */
#include <stdlib.h> /* free() */
#include <errno.h> /* ERANGE, EACCES */
#include <unistd.h> /* getcwd */
#include <limits.h>

#include "util.h"
#include "path.h"

#define PATH_INIT_SIZE 4096
#define IS_ROOT(path) ((path)->len == 1)

struct path *
path_at(const char *str)
{
	struct path *path;
	char *buf;

	/* XXX: resolved path == NULL is POSIX.1-2008 future, tcc failed at runtime */
	if ((buf = realpath(str, NULL)) == NULL)
		return NULL;

	path = xmalloc(sizeof(struct path));
	path->buf = buf;
	path->len = strlen(buf);
	path->cap = path->len + 1;
	return path;
}

struct path *
path_dup(const struct path *src)
{
	struct path *path;

	path = xcalloc(1, sizeof(struct path));
	path_copy(path, src);
	return path;
}

void
path_free(struct path *path)
{
	free(path->buf);
	free(path);
}

void
path_copy(struct path *dest, const struct path *src)
{
	if (dest->cap <= src->len) {
		dest->buf = xrealloc(dest->buf, src->cap);
		dest->cap = src->cap;
	}
	memcpy(dest->buf, src->buf, src->len + 1);
	dest->len = src->len;
}

struct path *
path_parent(struct path *path)
{
	size_t i;

	if (IS_ROOT(path)) {
		return NULL;
	}

	for (i = path->len - 1; path->buf[i] != '/'; --i);
	path->len = i ? i : 1;
	path->buf[path->len] = '\0';

	return path;
}

void
path_join(struct path *path, const char *str)
{
	size_t len;
	size_t total;
	int add;

	if (str == NULL) {
		return;
	}
	len = strlen(str);

	if (IS_ROOT(path)) {
		add = 0;
	} else {
		path->buf[path->len] = '/';
		add = 1;
	}
	total = path->len + len + add;
	if (total >= path->cap) {
		path->cap = total + 1;
		path->buf = xrealloc(path->buf, path->cap);
	}
	memcpy(path->buf + path->len + add, str, len + 1); /* len + '\0' */
	path->len = total;
}
