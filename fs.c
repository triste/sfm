#include <stdlib.h> /* free() */
#include <dirent.h> /* dirfd(), opendir(), readdir(), closedir(), struct dirent */
#include <sys/stat.h> /* stat, fstatat */
#include <errno.h> /* errno, EACCES */
#include <fcntl.h> /* AT_SYMLINK_NOFOLLOW */

#include "path.h"
#include "fs.h"
#include "hash.h"
#include "util.h"

#include <sys/inotify.h>

#define HT_INIT_EXPONENT 4
#define DIR_INIT_SIZE (1U << 4)

#define IS_DOT(X) (X[0] == '.' && (!X[1] || (X[1] == '.' && !X[2])))
#define DIR_SIZE(X) (sizeof(struct dir) + sizeof(struct file) * (X))

static int dircompare(const void *, const void *);
static size_t dirhash(const void *);

struct fs*
fs_create(void)
{
	struct htable *table;
	struct htconf conf;
	struct fs *fs;
	int fd;

	conf.cmp_f = dircompare;
	conf.hash_f = dirhash;
	table = ht_create(&conf, HT_INIT_EXPONENT);
	fd = inotify_init();

	fs = xmalloc(sizeof(struct fs));
	fs->table = table;
	fs->fd = fd;
	fs->wdirs = NULL;
	fs->wdnum = 0;
	return fs;
}

void
fs_free(struct fs *fs)
{
	struct dir *dir;
	size_t i, j;

	for (dir = ht_first(fs->table, &i); dir; dir = ht_next(fs->table, &i)) {
		for (j = 0; j < dir->fsize_all; ++j)
			free(dir->files[j].name);
		free(dir);
	}
	ht_free(fs->table);
	free(fs);
}

struct dir *
fs_watch(struct fs *fs, const char *path)
{
	size_t i;
	struct stat st;
	struct dir tmp;
	struct dir *dir;
	struct file *file;
	DIR *ds;
	struct dirent *de;
	int fd;

	if (stat(path, &st) == -1)
		return NULL;

	fs->wdirs = xrealloc(fs->wdirs, (fs->wdnum + 1) * sizeof(struct watchdir));
	fs->wdirs[fs->wdnum++].wd  = inotify_add_watch(fs->fd, path, IN_CREATE | IN_DELETE);

	tmp.ino = st.st_ino;
	tmp.dev = st.st_dev;

	i = ht_lookup(fs->table, &tmp);
	if ((dir = ht_find(fs->table, i)) != NULL)
		return dir;

	if ((ds = opendir(path)) == NULL) {
		if (errno != EACCES)
			return NULL;
		dir = xmalloc(sizeof(struct dir));
		dir->fsize_all = dir->fcap = 0;
	} else {
		fd = dirfd(ds);
		dir = xmalloc(DIR_SIZE(DIR_INIT_SIZE));
		dir->fcap = DIR_INIT_SIZE;

		for (dir->fsize_all = 0; (de = readdir(ds)) != NULL;) {
			if (IS_DOT(de->d_name))
				continue;
			if (dir->fsize_all == dir->fcap) {
				dir->fcap *= 2;
				dir = xrealloc(dir, DIR_SIZE(dir->fcap));
			}
			file = dir->files + dir->fsize_all;
			/* XXX optimisation: use string chunk */
			file->name = xstrdup(de->d_name);
			fstatat(fd, de->d_name, &st, AT_SYMLINK_NOFOLLOW);
			file->mode = st.st_mode;
			if (S_ISLNK(file->mode))
				fstatat(fd, de->d_name, &st, 0);
			file->ino = st.st_ino;
			file->dev = st.st_dev;

			dir->fsize_all++;
		}
		closedir(ds);
	}
	dir->fsize = dir->fsize_all; /* TODO: filter */
	dir->cfile = dir->cline = 0;
	dir->ino = tmp.ino;
	dir->dev = tmp.dev;
	ht_insert(fs->table, i, dir);
	return dir;
}

void
dir_rebuild(struct dir *dir)
{
}

void
dir_increate(struct dir *dir, const char *file)
{
	if (file == NULL) {
		/* Monitoring system doesn't provide file name */
		dir_rebuild(dir);
		return;
	}
	dir_rebuild(dir); /* TODO: insert file in directory */
}

void
dir_indelete(struct watchdir *wdir, const char *file)
{
	if (file == NULL) {
		/* Monitoring system doesn't provide file name */
		dir_rebuild(wdir->dir);
		return;
	}
	dir_rebuild(wdir->dir); /* TODO: delete file from directory */
}

void
fs_unwatch(struct fs *fs, struct dir *dir)
{
	(void)dir;
	(void)fs;

	/* TODO: inotify/kqueue */
	return;
}

void
dir_setcfile(struct dir *dir, ino_t ino, dev_t dev)
{
	size_t i;
	struct file *file;

	if (dir == NULL)
		return;

	file = dir->files + dir->cfile;
	if (file->ino == ino && file->dev == dev)
		return;

	for (dir->cfile = dir->cline = i = 0; i < dir->fsize; ++i) {
		file = dir->files + i;
		if (file->ino == ino && file->dev == dev) {
			dir->cfile = dir->cline = i;
			return;
		}
	}
}

int
dircompare(const void *dir1, const void *dir2)
{
	const struct dir *a = dir1;
	const struct dir *b = dir2;

	return a->ino == b->ino && a->dev == b->dev;
}

size_t
dirhash(const void *data)
{
	return ((const struct dir *)data)->ino;
}

