#include <sys/stat.h>

struct file {
	char *name; /* XXX optimization: use string chunk instead */

	/* Info about the link */
	mode_t mode;

	/* Info about the file the link references */
	ino_t ino;
	dev_t dev;
};

struct dir {
	ino_t ino;
	dev_t dev;

	size_t cfile;
	size_t cline;

	size_t fsize_all;
	size_t fsize;
	size_t fcap;
	struct file files[1];
};

struct watchdir {
	char *path;
	struct dir *dir;
	int wd;
};

struct fs {
	struct htable *table;
	int fd;
	struct watchdir *wdirs;
	size_t wdnum;
};

struct fs *fs_create(void);
void fs_free(struct fs *);

struct dir *fs_watch(struct fs *, const char *);
void fs_unwatch(struct fs *, struct dir *);

void dir_setcfile(struct dir *, ino_t, dev_t);

#define dir_denied(dir) ((dir)->fcap == 0)
#define dir_empty(dir) ((dir)->fsize == 0)


struct fs_ {
	struct dir *dirs;
	struct dir *preview;
};
