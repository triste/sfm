struct cell;

struct term * term_create(void);
void term_free(struct term *);

void term_raw(struct term *, int);
void term_render(struct term *, const struct cell *, size_t);
void term_size(struct term *, unsigned short *, unsigned short *);
char term_getchar(struct term *);
