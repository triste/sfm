include config.mk

SRC = sfm.c ui.c term.c fs.c path.c preview.c hash.c map.c util.c
OBJ = ${SRC:.c=.o}

all: options sfm

options:
	@echo sfm build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

config.h:
	@echo creating $@ from config.def.h
	@cp config.def.h $@

sfm: ${OBJ}
	@echo CC -o $@
	@${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f sfm ${OBJ}

.PHONY: all options clean
