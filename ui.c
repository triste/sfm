#include <string.h> /* mem*() */
#include <stdlib.h> /* free() */
#include <locale.h> /* setlocale(), LC_CTYPE */
#include <wchar.h> /* wcwidth() */
#include <stdio.h> /* EOF */

#include "sfm.h"
#include "path.h"
#include "preview.h"
#include "fs.h"
#include "term.h"
#include "util.h"

#include "ui.h"

static void drawfile(struct window *, struct preview *);
static void drawdir(struct window *, struct dir *);
static void drawline(struct window *, int, int, const char *);

struct ui *
ui_create(struct sfm *sfm)
{
	struct ui *ui;
	struct term *term;
	struct window *wins;

	(void) setlocale(LC_CTYPE, "");

	term = term_create();
	wins = xcalloc(sfm->dsize, sizeof(struct window));

	ui = xmalloc(sizeof(struct ui));
	ui->sfm = sfm;
	ui->term = term;
	ui->wins = wins;
	ui->cells = NULL;
	return ui;
}

void
ui_free(struct ui *ui)
{
	term_free(ui->term);
	free(ui->wins);
	free(ui->cells);
	free(ui);
}

void
ui_suspend(struct ui *ui)
{
	term_raw(ui->term, 0);
}

void
ui_resume(struct ui *ui)
{
	term_raw(ui->term, 1);
}

void
ui_draw(struct ui *ui)
{
	unsigned int i;

	memset(ui->cells, 0, sizeof(ui->cells[0]) * ui->cnum);
	for (i = 0; i < ui->sfm->dsize; ++i)
		drawdir(&ui->wins[i], ui->sfm->dirs[i]);
	drawfile(&ui->wins[ui->sfm->dsize-1], ui->sfm->preview);
	term_render(ui->term, ui->cells, ui->cnum);
}

/* TODO: <C-*>, <ESC>, etc */
const char *
ui_getkey(struct ui *ui)
{
	static char buf[2];
	char ch;

	if ((ch = term_getchar(ui->term)) == EOF)
		return NULL;

	buf[0] = ch;
	return buf;
}

int
ui_resize(struct ui *ui)
{
	struct window *wins = ui->wins;
	size_t i;
	size_t sum;
	unsigned short rows, cols;
	unsigned short y;

	term_size(ui->term, &rows, &cols);
	ui->cnum = rows * cols;

	ui->cells = xrealloc(ui->cells, ui->cnum * sizeof(struct cell));

	for (i = sum = 0; i < ui->sfm->dsize; ++i) {
		sum += ui->sfm->ratios[i];
		wins[i].h = rows;
		wins[i].lines = xrealloc(wins[i].lines, rows * sizeof(struct cell *));
	}
	wins[0].x = 0;
	for (i = 0; i < ui->sfm->dsize - 1; ++i) {
		wins[i].w = ui->sfm->ratios[i] * cols / sum;
		wins[i+1].x = wins[i].x + wins[i].w;
		wins[i].w--; /* indent */
	}
	wins[i].w = cols - wins[i].x;

	for (i = 0; i < ui->sfm->dsize; ++i) {
		for (y = 0; y < wins[i].h; ++y) {
			wins[i].lines[y] = ui->cells + (y + wins[i].y) * cols + wins[i].x;
		}
	}

	return 0;
}

void
drawdir(struct window *win, struct dir *dir)
{
	size_t i;
	int y;

	if (dir == NULL)
		return;

	if (dir_denied(dir)) {
		drawline(win, 0, 0, "Permission denied");
		return;
	}
	if (dir_empty(dir)) {
		drawline(win, 0, 0, "Empty");
		return;
	}

	if (dir->cline >= win->h)
		dir->cline = win->h - 1;
	i = dir->cfile - dir->cline;
	for (y = 0; y < win->h && i < dir->fsize; ++i, ++y)
		drawline(win, 2, y, dir->files[i].name);
	drawline(win, 0, dir->cline, ">");
}

void
drawfile(struct window *win, struct preview *preview)
{
	size_t i, n;
	size_t y;

	n = preview_preload(preview, win->h);
	for (y = i = 0; i < n && y < win->h; ++i)
		drawline(win, 0, y++, preview->lines[i]);
}

void
drawline(struct window *win, int x, int y, const char *str)
{
	struct cell *line;
	int len;
	wchar_t wchar;
	int width;

	line = win->lines[y];
	for (width = 0; x < win->w; x += width, str += len) {
		if ((len = mbtowc(&wchar, str, MB_CUR_MAX)) == 0)
			return;
		if (len < 0) {
			mbtowc(NULL, NULL, MB_CUR_MAX);
			wchar = *str;
			len = 1;
		}
		if ((width = wcwidth(wchar)) < 0) {
			if (wchar == '\t') {
				line[x].len = 0;
				/* TODO: use `it` escape code */
				line[x].width = width = 8;
			} else {
				line[x].data[0] = '?';
				line[x].len = 1;
				line[x].width = width = 1;
			}
		} else if (width == 0) {
			/* TODO: handle combining character */
			continue;
		} else {
			memcpy(line[x].data, str, len);
			line[x].len = len;
			line[x].width = width;
		}
	}

	if (*str != '\0') {
		x -= width;
		line[x].data[0] = '~';
		line[x].len = 1;
		line[x].width = 1;
	}
}
