struct htconf {
	int (*cmp_f)(const void *, const void *);
	size_t (*hash_f)(const void *);
};

struct htable {
	void **data;
	size_t size;
	size_t used;

	struct htconf conf;
};

struct htable *ht_create(struct htconf *, unsigned int);
void ht_free(struct htable *);

size_t ht_lookup(struct htable *, void *);
void *ht_find(struct htable *, size_t);
void ht_insert(struct htable *, size_t, void *);

void *ht_first(struct htable *, size_t *);
void *ht_next(struct htable *, size_t *);

/* XXX: use ht_iterate(struct htable *, int (*handle)(void *entry)) */
